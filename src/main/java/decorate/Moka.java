package decorate;

public class Moka extends CondimentDecorator {
    public Moka(Beverage beverage) {
        super(beverage);
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + "+摩卡";
    }

    public double cost() {
        return beverage.cost() + 1;
    }
}
