package decorate;

public class Sugar extends CondimentDecorator {
    public Sugar(Beverage beverage) {
        super(beverage);
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + "+糖";
    }

    public double cost() {
        return beverage.cost() + 0.1;
    }
}
