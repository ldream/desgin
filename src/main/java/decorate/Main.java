package decorate;

public class Main {
    public static void main(String[] args) {
        Beverage tea = new Tea();
        tea = new Sugar(tea);
        System.out.println(tea.cost());
        System.out.println(tea.getDescription());


        Beverage coffee = new Coffee();
        coffee = new Sugar(coffee);
        coffee = new Moka(coffee);
        System.out.println(coffee.cost());
        System.out.println(coffee.getDescription());
    }
}
