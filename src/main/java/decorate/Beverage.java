package decorate;

/**
 * 饮料
 */
public abstract class Beverage {
    protected String description = "unkonw beverage";

    public String getDescription() {
        return description;
    }

    public abstract double cost();
}