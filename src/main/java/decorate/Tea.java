package decorate;

public class Tea extends Beverage {
    public Tea() {
        description = "茶";
    }

    public double cost() {
        return 20;
    }
}
