package component;


import java.util.Iterator;
import java.util.Stack;

public class ComponentIterator implements Iterator {
    private Stack stack = new Stack();

    public ComponentIterator(Iterator iterator) {
        stack.push(iterator);
    }

    public boolean hasNext() {
        if (stack.empty()) {
            return false;
        } else {
            Iterator i = (Iterator) stack.peek();
            if (i.hasNext()) {
                return true;
            } else {
                stack.pop();
                return hasNext();
            }
        }
    }

    public Component next() {
        if (hasNext()) {
            Iterator i = (Iterator) stack.peek();
            Component c = (Component) i.next();
            if (c instanceof Menu) {
                stack.push(c.createIterator());
            }
            return c;
        }
        return null;
    }

    public void remove() {
        throw new UnsupportedOperationException();

    }
}
