package component;

import java.util.ArrayList;
import java.util.Iterator;

public class Menu extends Component {
    private ArrayList<Component> components = new ArrayList<Component>();
    String name;
    String description;

    public Menu(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public void add(Component c) {
        components.add(c);
    }

    @Override
    public void remove(Component c) {
        components.remove(c);
    }

    @Override
    public Component getChild(int i) {
        return components.get(i);
    }

    @Override
    public void print() {
        System.out.println(String.format("name:%s description:%s", name, description));
        Iterator<Component> i = components.iterator();
        while (i.hasNext()) {
            i.next().print();
        }

    }

    public Iterator<Component> createIterator() {
        return new ComponentIterator(components.iterator());
    }
}
