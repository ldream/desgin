package component;

import java.util.Iterator;

public class MenuItem extends Component {
    String name;
    String description;
    boolean vegetarian;
    float price;

    public MenuItem(String name, String description, boolean vegetarian, float price) {
        this.name = name;
        this.description = description;
        this.vegetarian = vegetarian;
        this.price = price;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean isVegetarian() {
        return vegetarian;
    }

    @Override
    public float getPrice() {
        return price;
    }

    @Override
    public void print() {
        System.out.println(this.toString());
    }

    public Iterator<Component> createIterator() {
        return new NullIterator();
    }

    @Override
    public String toString() {
        return "MenuItem{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", vegetarian=" + vegetarian +
                ", price=" + price +
                '}';
    }
}
