package chain;

public class Main {

    public static void main(String[] args) {
        RecommendRequest recommendRequest = new RecommendRequest(1L, 10);

        Handler a = new UserControlHandler();
        Handler b = new AIHandler();
        Handler c = new CallBackHandler();
        a.setSuccessor(b);
        b.setSuccessor(c);

        a.chain(recommendRequest);
    }
}
