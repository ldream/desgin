package chain;

public abstract class Handler {

    public Handler successor;


    protected void chain(RecommendRequest request) {
        if (request.needContinus()) {
            handleRequest(request);
            if (successor != null)
                successor.chain(request);
        }

    }

    protected abstract void handleRequest(RecommendRequest request);

    public Handler getSuccessor() {
        return successor;
    }

    public void setSuccessor(Handler successor) {
        this.successor = successor;
    }
}
