package chain;

import java.util.HashMap;
import java.util.List;

//推荐策略
public class RecommendRequest {
    private long strategyId;
    private HashMap<Object, Integer> recommendLocate = new HashMap<Object, Integer>();
    private HashMap<Integer, Object> locateRecommend = new HashMap<Integer, Object>();

    private List<Object> preRecommends;
    private int count;


    public void fixRecommend(Object object, Integer locate) {
        recommendLocate.put(object, locate);
        locateRecommend.put(locate, object);
    }

    public boolean isFinish() {
        return count == recommendLocate.size();
    }

    public boolean needContinus() {
        if (count == recommendLocate.size()) {
            return false;
        } else if (preRecommends != null && preRecommends.size() <= count - recommendLocate.size()) {
            return false;
        }
        return true;
    }

    public RecommendRequest(long strategyId, int count) {
        this.strategyId = strategyId;
        this.count = count;
    }

    public long getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(long strategyId) {
        this.strategyId = strategyId;
    }

    public HashMap<Object, Integer> getRecommendLocate() {
        return recommendLocate;
    }

    public void setRecommendLocate(HashMap<Object, Integer> recommendLocate) {
        this.recommendLocate = recommendLocate;
    }

    public HashMap<Integer, Object> getLocateRecommend() {
        return locateRecommend;
    }

    public void setLocateRecommend(HashMap<Integer, Object> locateRecommend) {
        this.locateRecommend = locateRecommend;
    }

    public List<Object> getPreRecommends() {
        return preRecommends;
    }

    public void setPreRecommends(List<Object> preRecommends) {
        this.preRecommends = preRecommends;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
