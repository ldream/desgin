package exterior;

public class Main {
    public static void main(String[] args) {
        A a = new A1();
        B b = new B1();
        C c = new C1();
        D d = new D1();
        Exterior e = new Exterior(a, b, c, d);
        e.enjoy();

    }
}
