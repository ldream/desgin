package exterior;

public class Exterior {
    private A a;
    private B b;
    private C c;
    private D d;

    public Exterior(A a, B b, C c, D d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public void enjoy() {
        a.a();
        b.b();
        c.c();
        d.d();
    }
}
