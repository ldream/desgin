package status;

public class TrunOverStatus extends Status {
    public void read(Book book) {
        System.out.println("读书中");
        book.setStatus(new ReadStatus());
    }

    public void close(Book book) {
        System.out.println("关闭书籍");
        book.setStatus(new CloseStatus());
    }


    public String current() {
        return "翻阅中";
    }
}
