package status;

public class Book {
    private Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Book(Status status) {
        this.status = status;
    }

    public void read() {
        status.read(this);
    }

    public void close() {
        status.close(this);
    }

    public void turnover() {
        status.turnover(this);
    }

    public void open() {
        status.open(this);
    }
}

