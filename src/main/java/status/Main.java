package status;

public class Main {
    public static void main(String[] args) {
        Book book = new Book(new CloseStatus());
        book.open();
        System.out.println(book.getStatus().current());
        book.read();
        System.out.println(book.getStatus().current());
        book.close();
        System.out.println(book.getStatus().current());
    }
}
