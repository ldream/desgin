package status;

public abstract class Status {
    public void read(Book book) {
        System.out.println(current() + " can not do this");
    }

    public void close(Book book) {
        System.out.println(current() + " can not do this");
    }

    public void turnover(Book book) {
        System.out.println(current() + " can not do this");
    }

    public void open(Book book) {
        System.out.println(current() + " can not do this");
    }

    public abstract String current();
}
