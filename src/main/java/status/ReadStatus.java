package status;

public class ReadStatus extends Status {


    public void close(Book book) {
        System.out.println("关闭书籍");
        book.setStatus(new CloseStatus());
    }

    public void turnover(Book book) {
        System.out.println("翻阅书籍");
        book.setStatus(new TrunOverStatus());
    }


    public String current() {
        return "读书中";
    }
}
