package status;

public class CloseStatus extends Status {


    public void open(Book book) {
        System.out.println("打开书籍");
        book.setStatus(new OpenStatus());
    }

    public String current() {
        return "关闭状态";
    }
}
