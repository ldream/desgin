package status;

public class OpenStatus extends Status {
    public void read(Book book) {
        System.out.println("读书中");
        book.setStatus(new ReadStatus());
    }

    public void close(Book book) {
        System.out.println("关闭书籍");
        book.setStatus(new CloseStatus());
    }

    public void turnover(Book book) {
        System.out.println("翻阅书籍");
        book.setStatus(new TrunOverStatus());
    }


    public String current() {
        return "打开状态";
    }
}
