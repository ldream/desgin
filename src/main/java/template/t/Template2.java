package template.t;

public class Template2 extends Template {
    private boolean need = true;

    public boolean isNeed() {
        return need;
    }

    public void setNeed(boolean need) {
        this.need = need;
    }

    protected void b() {
        System.out.println("33333333");
    }

    protected void c() {
        System.out.println("4444444");
    }

    boolean need() {
        return need;
    }
}
