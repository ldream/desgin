package template.t;

public class Template1 extends Template {
    private boolean need = true;

    public boolean isNeed() {
        return need;
    }

    public void setNeed(boolean need) {
        this.need = need;
    }

    protected void b() {
        System.out.println("11111");
    }

    protected void c() {
        System.out.println("22222");
    }

    @Override
     boolean need() {
        return need;
    }
}
