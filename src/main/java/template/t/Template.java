package template.t;

public abstract class Template {

    protected void a() {
        System.out.println("aaaaa");
    }


    protected abstract void b();

    protected abstract void c();

    protected void d() {
        System.out.println("ddddd");
    }

    boolean need() {
        return true;
    }

    public final void execute() {
        a();
        b();
        c();
        if (need()) {
            d();
        }

    }

}
