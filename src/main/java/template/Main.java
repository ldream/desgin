package template;

import template.t.Template1;
import template.t.Template2;

public class Main {
    public static void main(String[] args) {
        Template1 template = new Template1();
        template.setNeed(false);
        template.execute();


        System.out.println();
        System.out.println();
        System.out.println();
        Template2 template2 = new Template2();
        template2.execute();
    }
}
