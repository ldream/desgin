package factory;

public class Persion {
    private AbstractFactory factory;
    private Phone phone;
    private PC pc;

    public Persion(AbstractFactory factory) {
        setFactory(factory);
    }


    public void setFactory(AbstractFactory factory) {
        this.factory = factory;
        this.phone = factory.createPhone();
        this.pc = factory.createPC();
    }

    public void makeCall() {
        phone.makeCall();
    }

    public void play() {
        pc.play();
    }
}
