package factory;

public class PingguoFactory implements AbstractFactory {
    public Phone createPhone() {
        return new IPhone();
    }

    public PC createPC() {
        return new Mac();
    }
}
