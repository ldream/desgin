package factory;

public interface AbstractFactory {
    public Phone createPhone();

    public PC createPC();
}
