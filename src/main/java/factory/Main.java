package factory;

public class Main {
    public static void main(String[] args) {
        System.out.println("简单工厂模式");
        Phone phone = SimpleFactory.createPhone(TypeEnum.HUAWEI);
        phone.makeCall();


        System.out.println("工厂模式");
        /**
         * 面向类,具体由子类实现
         */
        AbstractFactory factory = new PingguoFactory();
        phone.makeCall();


        System.out.println("抽象工厂模式");
        Persion xiaoming = new Persion(factory);
        xiaoming.makeCall();
        xiaoming.play();


        xiaoming.setFactory(new HuaweiFactory());
        xiaoming.makeCall();
        xiaoming.play();

    }
}
