package factory;

public class SimpleFactory {
    public static Phone createPhone(TypeEnum typeEnum) {
        Phone phone = null;
        switch (typeEnum) {
            case HUAWEI:
                phone = new HuaweiPhone();
                break;
            case PINGGUO:
                phone = new IPhone();
                break;
        }
        return phone;
    }
}
