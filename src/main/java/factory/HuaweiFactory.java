package factory;

public class HuaweiFactory implements AbstractFactory {
    public Phone createPhone() {
        return new HuaweiPhone();
    }

    public PC createPC() {
        return new HuaweiPc();
    }
}
