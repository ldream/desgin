package proxy;

import java.lang.reflect.Proxy;

public class Main {
    public static void main(String[] args) {
        PersonBean person = new PersonBeanImpl();
        person = getOwnerProxy(person);
        person.setName("张三");
        try {
            person.setHotOrNotRating(10);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(person.getName());
        System.out.println(person.getHotOrNotRating());

        PersonBean person2 = new PersonBeanImpl();
        person2 = getNonOwnerProxy(person2);
        try {
            person2.setName("李四");
        } catch (Exception e) {
            e.printStackTrace();
        }
        person2.setHotOrNotRating(10);
        System.out.println(person2.getName());
        System.out.println(person2.getHotOrNotRating());

    }


    public static PersonBean getOwnerProxy(PersonBean person) {
        return (PersonBean) Proxy.newProxyInstance(person.getClass().getClassLoader(), person.getClass().getInterfaces(), new OwnerInvocationHandler(person));
    }

    public static PersonBean getNonOwnerProxy(PersonBean person) {
        return (PersonBean) Proxy.newProxyInstance(person.getClass().getClassLoader(), person.getClass().getInterfaces(), new NonOwnerInvocationHandler(person));
    }

}
