package command;

public class CommandOn implements Command {
    private Light light;

    public CommandOn(Light light) {
        this.light = light;
    }

    public void execute() {
        light.on();
    }
}
