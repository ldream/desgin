package command;

import java.util.Date;

public class Light {
    public void on() {
        System.out.println("开灯" + new Date());
    }

    public void off() {
        System.out.println("关灯" + new Date());
    }
}
