package command;

public class Main {
    public static void main(String[] args) {
        Light light = new Light();

        Command on = new CommandOn(light);
        Command off = new CommandOff(light);
        Command delay = new CommandDelay(5000);
        Command compose = new CommandCompose(new Command[]{on, delay, off});


        compose.execute();

    }
}
