package command;

public class CommandOff implements Command {
    private Light light;

    public CommandOff(Light light) {
        this.light = light;
    }

    public void execute() {
        light.off();
    }
}
