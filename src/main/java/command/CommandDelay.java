package command;

public class CommandDelay implements Command {
    private long sleep;

    public CommandDelay(long sleep) {
        this.sleep = sleep;
    }

    public void execute() {
        try {
            Thread.currentThread().sleep(sleep);
        } catch (InterruptedException e) {
            

        }
    }
}
