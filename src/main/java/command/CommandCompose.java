package command;

public class CommandCompose implements Command {
    private Command[] list;

    public CommandCompose(Command[] list) {
        this.list = list;
    }

    public void execute() {
        for (Command c : list) {
            c.execute();
        }
    }
}
