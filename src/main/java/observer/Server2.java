package observer;

import java.util.Observable;
import java.util.Observer;

public class Server2 implements Observer {
    public void update(Observable o, Object arg) {
        System.out.println(o);
        System.out.println(arg);
    }
}
