package observer;

public class Main {
    public static void main(String[] args) {
        Client c = new Client();
        Server1 s1 = new Server1();
        Server2 s2 = new Server2();

        c.addObserver(s1);
        c.addObserver(s2);

        c.setAa("123123");

        c.notifyObservers();
    }
}
