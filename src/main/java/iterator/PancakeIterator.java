package iterator;

import java.util.ArrayList;

public class PancakeIterator implements Iterator<MenuItem> {
    private ArrayList list;
    private java.util.Iterator<MenuItem> iterator;

    public PancakeIterator(ArrayList list) {
        this.list = list;
        this.iterator = list.iterator();
    }

    public boolean hasNext() {
        return iterator.hasNext();
    }

    public MenuItem next() {
        return iterator.next();
    }
}
