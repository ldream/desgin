package iterator;

import exterior.A;

import java.util.ArrayList;
import java.util.Random;

public class PancakeMenu implements Menu {
    private ArrayList menuItems = new ArrayList();

    Random random = new Random();


    public Iterator createIterator() {
        return new PancakeIterator(menuItems);
    }


    public PancakeMenu() {
        for (int i = 1; i < 6; i++) {
            addItem("pancake_" + i, "pancake_desc_" + 1, random.nextBoolean(), random.nextFloat());
        }
    }

    public void addItem(String name, String description, boolean vegetarian, float price) {
        MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
        menuItems.add(menuItem);
    }

}
