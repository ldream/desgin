package iterator;

public class Main {
    public static void main(String[] args) {
        DinerMenu dinerMenu = new DinerMenu();
        PancakeMenu pancakeMenu = new PancakeMenu();
        Waiter waiter = new Waiter(dinerMenu, pancakeMenu);
        waiter.print();
    }

}
