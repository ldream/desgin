package iterator;

public class DinerIterator implements Iterator<MenuItem> {
    private MenuItem[] items;
    private int position = 0;

    public DinerIterator(MenuItem[] items) {
        this.items = items;
    }

    public boolean hasNext() {
        if (position >= items.length || items[position] == null) {
            return false;
        }
        return true;
    }

    public MenuItem next() {
        MenuItem item = items[position];
        position++;
        return item;
    }
}
