package iterator;

import java.util.ArrayList;
import java.util.Random;

public class DinerMenu implements Menu {
    private MenuItem[] menuItems;
    private static final int MAX_ITEMS = 10;
    private int numberOfItems = 0;


    public Iterator createIterator() {
        return new DinerIterator(menuItems);
    }

    public DinerMenu() {
        menuItems = new MenuItem[MAX_ITEMS];
        Random random = new Random();
        for (int i = 1; i < 6; i++) {
            addItem("diner_" + i, "diner_desc_" + 1, random.nextBoolean(), random.nextFloat());
        }
    }

    public void addItem(String name, String description, boolean vegetarian, float price) {
        if (numberOfItems >= MAX_ITEMS) {
            throw new RuntimeException("out of menu size");
        }
        MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
        menuItems[numberOfItems] = menuItem;
        numberOfItems++;
    }

    public MenuItem[] getItems() {
        return menuItems;
    }
}
