package iterator;

public class Waiter {
    private Menu dinerMenu;
    private Menu pancakeMenu;

    public Waiter(Menu dinerMenu, Menu pancakeMenu) {
        this.dinerMenu = dinerMenu;
        this.pancakeMenu = pancakeMenu;
    }

    public void print() {
        Iterator iterator = dinerMenu.createIterator();
        print(iterator);
        iterator = pancakeMenu.createIterator();
        print(iterator);
    }

    public void print(Iterator iterator) {
        while (iterator.hasNext()) {
            System.out.println(iterator.next().toString());
        }
    }
}
