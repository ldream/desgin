package singleton;

public class Singletion3 {
    private static Singletion3 instance;

    private Singletion3() {
    }


    /**
     * 用于性能要求不严的的场景
     *
     * @return
     */
    public static Singletion3 getInstance() {
        if (instance == null) {
            synchronized (Singletion3.class) {
                if (instance == null) {
                    instance = new Singletion3();
                }
            }
        }
        return instance;
    }
}
