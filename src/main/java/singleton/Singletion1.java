package singleton;

public class Singletion1 {
    /**
     * jvm加载时就创建实例
     */
    private static Singletion1 instance = new Singletion1();

    private Singletion1() {
    }


    public static Singletion1 getInstance() {
        return instance;
    }
}
