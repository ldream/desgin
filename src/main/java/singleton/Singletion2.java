package singleton;

public class Singletion2 {
    private static Singletion2 instance;

    private Singletion2() {
    }


    /**
     * 用于性能要求不严的的场景
     * @return
     */
    public synchronized static Singletion2 getInstance() {
        if (instance == null) {
            instance = new Singletion2();
        }
        return instance;
    }
}
