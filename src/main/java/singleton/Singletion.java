package singleton;

public class Singletion {
    private static Singletion instance;

    private Singletion() {
    }


    public synchronized static Singletion getInstance() {
        if (instance == null) {
            instance = new Singletion();
        }
        return instance;
    }
}
