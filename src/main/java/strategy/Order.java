package strategy;

public abstract class Order {
    private Pay pay;

    public Order() {
        this.pay = new WxH5Pay();
    }

    public void setPay(Pay pay) {
        this.pay = pay;
    }

    public void orderPay() {
        pay.pay();
    }
}
