package strategy;

public class Main {
    public static void main(String[] args) {
        Order order = new UserOrder();
        order.orderPay();
        order.setPay(new WxMaPay());
        order.orderPay();
    }
}
