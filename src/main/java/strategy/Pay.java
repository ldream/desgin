package strategy;

public interface Pay {
    public void pay();
}
